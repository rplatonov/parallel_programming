﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LabWork
{
    interface IReadersWritersInterface
    {
        void reader(object threadId);
    
        void writer(object threadId);
    }
}
