﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace LabWork
{
    class ProblemWithEvents
    {
        static bool _isEmpty = true;
        static bool _finished;

        private const int IterationsCount = 5;

        const int NumberOfWriters = 5;
        const int NumberOfReaders = 5;
        static string _buffer = "";

        static int[] _index;
        static string[][] _writers;
        static List<string>[] _readers;
        static AutoResetEvent _full = new AutoResetEvent(true);
        static AutoResetEvent _empty = new AutoResetEvent(true);

        static void Main(string[] args)
        {
            _index = new int[NumberOfWriters];
            _writers = new string[NumberOfWriters][];
            _readers = new List<string>[NumberOfReaders];

            for (var i = 0; i < NumberOfReaders; i++)
            {
                _readers[i] = new List<string>();
            }
            for (var i = 0; i < NumberOfWriters; i++)
            {
                _index[i] = 0;
                _writers[i] = new string[IterationsCount];
                for (var j = 0; j < IterationsCount; j++)
                {
                    _writers[i][j] = Utils.WordFinder(i + 1);
                }
            }
            Thread[] readers = new Thread[NumberOfReaders];
            Thread[] writers = new Thread[NumberOfWriters];

            for (var i = 0; i < NumberOfReaders; i++)
                readers[i] = new Thread(Read);

            for (var j = 0; j < NumberOfWriters; j++)
                writers[j] = new Thread(Write);

            // Запускаем читателей и писателей
            for (var i = 0; i < NumberOfReaders; i++)
            {
                readers[i].Name = i.ToString();
                readers[i].Start(new object[] {i, _full, _empty});
            }
            for (int i = 0; i < NumberOfWriters; i++)
            {
                writers[i].Name = i.ToString();
                writers[i].Start(new object[] {i, _full, _empty});
            }
            // Ожидаем завершения работы писателей
            for (var i = 0; i < NumberOfWriters; i++)
                writers[i].Join();
            // Сигнал о завершении работы для читателей
            _finished = true;
            // Ожидаем завершения работы читателей
            for (var i = 0; i < NumberOfReaders; i++)
                readers[i].Join();

            Console.Read();
        }

        static void Read(object n)
        {
            var state = n as object[];
            if (state == null) return;
            var i = (int)state[0];
            var evFull = state[1] as AutoResetEvent;
            var evEmpty = state[2] as AutoResetEvent;

            while (true)
            {
                //Thread.Sleep(100);
                evFull?.WaitOne();
                if (_finished) break;
                _readers[i].Add(_buffer);
                Console.WriteLine("Thread {0}; reads message: {1}", i + 1 + NumberOfWriters,
                    _readers[i].Last());
                evEmpty?.Set();
            }
        }

        static void Write(object n)
        {
            var state = n as object[];
            if (state == null) return;
            var i = (int) state[0];
            var evFull = state[1] as AutoResetEvent;
            var evEmpty = state[2] as AutoResetEvent;
            while (_index[i] < IterationsCount)
            {
                //Thread.Sleep(100);
                evEmpty?.WaitOne();
                _buffer = _writers[i][_index[i]++];
                Console.WriteLine("Thread {0}; writes message: {1}", i + 1, _buffer);
                evFull?.Set();
            }
        }

        
    }
}