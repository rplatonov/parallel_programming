﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace LabWork
{
    class ProblemWithLocks
    {
        static bool _isEmpty = true;
        static bool _finished;

        static int IterationsCount = 5;

        const int NumberOfWriters = 5;
        const int NumberOfReaders = 5;

        static string _buffer = "";
        
        static int[] _index;
        static string[][] _writers;
        static List<string>[] _readers;

        static void Main_1(string[] args)
        {            
            _index = new int[NumberOfWriters];
            _writers = new string[NumberOfWriters][];
            _readers = new List<string>[NumberOfReaders];

            for (var i = 0; i < NumberOfReaders; i++)
            {
                _readers[i] = new List<string>();
            }

            for (var i = 0; i < NumberOfWriters; i++)
            {
                _index[i] = 0;
                _writers[i] = new string[IterationsCount];
                for (var j = 0; j < IterationsCount; j++)
                {
                    _writers[i][j] = Utils.WordFinder(i + 1);
                }
            }

            Thread[] readers = new Thread[NumberOfReaders];
            Thread[] writers = new Thread[NumberOfWriters];


            for (var i = 0; i < NumberOfReaders; i++)
                readers[i] = new Thread(Read);

            for (var j = 0; j < NumberOfWriters; j++)
                writers[j] = new Thread(Write);
            // Запускаем читателей и писателей
            for (var i = 0; i < NumberOfReaders; i++)
            {
                readers[i].Start(i);
            }
            for (var i = 0; i < NumberOfWriters; i++)
            {
                writers[i].Start(i);
            }
            // Ожидаем завершения работы писателей
            for (var i = 0; i < NumberOfWriters; i++)
            {
                writers[i].Join();
            }

            // Сигнал о завершении работы для читателей
            _finished = true;

            // Ожидаем завершения работы читателей
            for (var i = 0; i < NumberOfReaders; i++)
            {
                readers[i].Join();
            }

            Console.Read();
        }

        static void Read(object n)
        {
            while (!_finished)
            {
                if (_isEmpty) continue;
                lock ("read")
                {
                    if (_isEmpty) continue;
                    //Thread.Sleep(100);
                    _readers[(int)n].Add(_buffer);
                    Console.WriteLine("Thread {0}; reads message: {1}" , (int)n + 1 + NumberOfWriters, _readers[(int)n].Last());
                    _isEmpty = true;
                }
            }
        }

        static void Write(object n)
        {
            while (_index[(int)n] < IterationsCount)
            {
                lock ("write")
                {
                    if (!_isEmpty) continue;
                    //Thread.Sleep(100);
                    _buffer = _writers[(int)n][_index[(int)n]++];
                    Console.WriteLine("Thread {0}; writes message: {1}", (int)n + 1, _buffer);
                    _isEmpty = false;
                }

            }
        }
       
    }
}
