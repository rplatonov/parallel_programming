package ru.kai.solutions.solutions;

/*
 * Name : ReaderWriterInterface.java
 * Author : knar@
 * Methods : reader and writer methods to be implemented..
 * 
 */

public interface ReadersWriterInterface {

	public void reader(String s) throws InterruptedException;

	public void writer(String s, Integer val) throws InterruptedException;

}
